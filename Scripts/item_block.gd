extends Area2D

var hit_already = false
var hit = false

var player_vars
# Called when the node enters the scene tree for the first time.
func _ready():
	player_vars = get_node("/root/PlayerVars")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_body_entered(body):
	if body.name == "Player" && hit == false:
		player_vars.score += 1
		hit = true
		print(self.name, " was hit by ", body.name)
		$AudioStreamPlayer2D.play(0.45)
		$AnimatedSprite2D.play("hit_top")
		#await get_tree().create_timer(0.3).timeout
		$CPUParticles2D.emitting = true
		$item_block_area.queue_free()
		$AnimatedSprite2D.queue_free()
		await get_tree().create_timer(0.4).timeout
		queue_free()
