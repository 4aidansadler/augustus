extends CanvasLayer

const PRESSED_COLOR = Color(.5, .5, .5, 1)
const NORMAL_COLOR = Color(1, 1, 1, 1)
##files----------------------
var Click1 = preload("res://SFX/click1.ogg")
var Click2 = preload("res://SFX/click2.ogg")

var touch_event = null
var state = {}
var player_vars
# Called when the node enters the scene tree for the first time.
func _ready():
	player_vars = get_node("/root/PlayerVars")
	var os_name = OS.get_name()
	if os_name.match("iOS"):
		$PauseMenu/QuitButton.visible = false
	elif os_name != "Windows":
		$"Virtual joystick left".visible = true
	else:
		$"Virtual joystick left".queue_free()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var PAUSE_BUTTON = $Buttons/pause_button
	var RESTART_BUTTON = $Buttons/restart_button
	if PAUSE_BUTTON.button_pressed == true:
		PAUSE_BUTTON.modulate = PRESSED_COLOR
	elif RESTART_BUTTON.button_pressed == true:
		RESTART_BUTTON.modulate = PRESSED_COLOR
	if Input.is_action_just_pressed("ui_cancel"):
		show_pause_menu()
	$GridContainer/ScoreLabel.text = str(player_vars.score)

func _unhandled_input(event):
	if event is InputEventScreenTouch:
		if event.pressed: # Down.
			state[event.index] = event.position
			touch_event = event
		else: # Up.
			state.erase(event.index)
			touch_event = null
	elif event is InputEventScreenDrag: # Movement.
		state[event.index] = event.position

func _on_pause_button_up():
	$Buttons/pause_button.modulate = NORMAL_COLOR
	show_pause_menu()

func _on_restart_button_up():
	get_tree().reload_current_scene()

func show_pause_menu():
	var pause_menu = $PauseMenu
	if pause_menu.visible == true:
		ui_sound(Click2)
		pause_menu.visible = false
	else:
		ui_sound(Click1)
		pause_menu.visible = true

func _on_quit_button_up():
	get_tree().quit()

func ui_sound(audiofile: Resource):
	var AUDIO_PLAYER = $AudioStreamPlayer
	if AUDIO_PLAYER.playing == false:
		AUDIO_PLAYER.stream = audiofile
		AUDIO_PLAYER.play()
