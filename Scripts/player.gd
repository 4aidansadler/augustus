extends CharacterBody2D

##Physics-------------------
@export var SPEED = 200.0
@export var JUMP_VELOCITY = -325.0
##Switches-------------------
var can_doublejump = false
var double_jumping = false
var repeat_stop = false
var hitting_block = false
var can_play_landed = false
##files----------------------
var LandingSound = preload("res://SFX/jumpland.wav")
##settings-------------------
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
##---------------------------

func _physics_process(delta):
	var touch_ui = get_node("TouchUI")
	if touch_ui.touch_event == null:
		repeat_stop = true
	# Handle Animation
	AnimationProcess()
	# Add the gravity.
	if not is_on_floor():
		can_play_landed = true
		velocity.y += gravity * delta
		if can_doublejump == true && double_jumping == false:
			if Input.is_action_just_pressed("ui_accept") || touch_ui.touch_event is InputEventScreenTouch && repeat_stop == true:
				velocity.y = JUMP_VELOCITY
				can_doublejump = false
				double_jumping = true
				can_play_landed = true
	else: 
		double_jumping = false
		if Input.is_action_just_pressed("ui_accept") || touch_ui.touch_event is InputEventScreenTouch:
			velocity.y = JUMP_VELOCITY
			can_doublejump = true
			repeat_stop = false
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("ui_left", "ui_right")
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
	move_and_slide()
func AnimationProcess():
	if !hitting_block:
		##Left-Right Movement---------------------------------
		var direction = Input.get_axis("ui_left", "ui_right")
		if direction < 0:
			$AnimatedSprite2D.flip_h = true
			if is_on_floor() && $AnimatedSprite2D.animation != "running":
				$AnimatedSprite2D.play("running")
		if direction > 0:
			$AnimatedSprite2D.flip_h = false
			if is_on_floor() && $AnimatedSprite2D.animation != "running":
				$AnimatedSprite2D.play("running")
		##----------------------------------------------------
		if direction == 0 && is_on_floor():
			$AnimatedSprite2D.play("Idle")
		elif !is_on_floor() && !double_jumping:
			$AnimatedSprite2D.play("Jump")
		elif !is_on_floor() && double_jumping:
			$AnimatedSprite2D.play("doublejump")


func _on_block_hitter_body_entered(body):
	for i in get_slide_collision_count():
		var collision = get_slide_collision(i)
		if can_play_landed == true && collision.get_collider().name == "Terrain" && $AudioStreamPlayer2D.playing == false:
			$AudioStreamPlayer2D.stream = LandingSound
			$AudioStreamPlayer2D.play()
			can_play_landed = false
	if body.name.match("item_block_area"):
		$AnimatedSprite2D.play("hit")
		hitting_block = true
		await get_tree().create_timer(0.2).timeout
		hitting_block = false
